const {createInvoice } = require('./invoice');

const invoice = {
  user: {
    name: "Dian Handiyansah",
    company: "KOTA DIGIVICE",
    city: "San Francisco",
    state: "CA",
    country: "US",
    postal_code: 94111
  },
  items: [
    {
      item: "Vitamin Rambut",
      year: "2020",
      quantity: 2,
      amount: 6000
    },
    {
      item: "Wardah",
      year: "2020",
      quantity: 1,
      amount: 2000
    },
    {
      item: "Sariayu",
      year: "2020",
      quantity: 1,
      amount: 2000
    },
    {
      item: "Bedak Wajah",
      year: "2020",
      quantity: 1,
      amount: 2000
    }
  ],
  subtotal: 12000,
  paid: 0,
  invoice_nr: 'INV/XI/21313'
};


createInvoice(invoice, "invoice.pdf")